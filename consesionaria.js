////////////FUNCIONALIDAD BOTON DESPLEGAR MENU EN TAMAÑO MOBIL/////////////////


var btnMenu = document.getElementById("btn-menu");
var menu = document.getElementById("menu");

///// Lo que hace es que cuando se hace click en el menu, se agrega o se quita la clase mostrar que contiene el la propiedad hight en 533px, haciendo que se despliegue  o cierre

btnMenu.addEventListener("click",function(){ 
    menu.classList.toggle("mostrar"); 
})




//////////GALERIA/SLIDER DE ARRIBA///////////


var header = document.querySelector("#header");
var n = 1;
var imgSlider = document.querySelector("#imgSliderJs");
var avanzar = document.querySelector("#avanzarJs");
var retroceder = document.querySelector("#retrocederJs");
var imagenesGaleriaArriba = document.querySelectorAll(".imagenSL");
var r = 1;
var punto0 = document.querySelector("#punto0");
var punto1 = document.querySelector("#punto1");
var punto2 = document.querySelector("#punto2");
var punto3 = document.querySelector("#punto3");
var Apuntos = [punto0,punto1,punto2,punto3];

    
///// Esta funcion intervalo es para que las imagenes de la galeria se vallan pasando solas cada 4 seg, y tambien que valla cambiando de color el punto de abajo de la galeria

setInterval(function(){
    
    // Esto es para que apenas se ejecuta la funcion me haga desaparecer la imagen que se esta mostrando actualmente en esa posicion de n para luego mostrar la que va en n++. Empiez con n y no con n-1, por que cuando se tiene que mostrar el primero que es n = 0 aca quedaria como 0-1 y esta mal

    imagenesGaleriaArriba[n].style.display = "none";
    n++;
    
    //Este condicional es para manejar el indice n de las imagenes asi no se pasa, e ir mostrando las imagenes y tambien aplicar el efecto cuando aparecen

    if(n<=3){  
        imagenesGaleriaArriba[n].style.display = "flex"; 
        imagenesGaleriaArriba[n].classList.add("efectoGaleriaArriba");
    }
    else{
        n=0;
    
        imagenesGaleriaArriba[n].style.display = "flex";
        imagenesGaleriaArriba[n].classList.add("efectoGaleriaArriba");   
    }
    

    // Esta es la parte de los puntos. Funciona igual que la anterior con la diferencia que en ves de poner display none se pone fondo blanco y borde azulcito

    Apuntos[r].style.background = "white";
    Apuntos[r].style.border = "2.0px solid #434E5E";
    
    r++;
    
    if(r<=3){ 
    Apuntos[r].style.background = "#434E5E";
    Apuntos[r].style.border = "2.0px solid white";
    }
    else{
        r=0;
        Apuntos[r].style.background = "#434E5E";
        Apuntos[r].style.border = "2.0px solid white";
    }
    
    },4000);



///// Esta es la funcion avanzar o pasar imagen de la galeria cuando se hace click en el la flecha que indica avanzar. Tambien me cambia la pintada al circulo de abajo. Funciona exactamente igual a la funcion de intervalo de arriba.

avanzar.addEventListener("click",function(){ 
      
    
    imagenesGaleriaArriba[n].style.display = "none"; 
    n++;
    
    if(n<=3){  
        imagenesGaleriaArriba[n].style.display = "flex";
        imagenesGaleriaArriba[n].classList.add("efectoGaleriaArriba");
     }    
    else{
        n=0;
        imagenesGaleriaArriba[n].style.display = "flex";
        imagenesGaleriaArriba[n].classList.add("efectoGaleriaArriba");
    }
    
    Apuntos[r].style.background = "white";
    Apuntos[r].style.border = "2.0px solid #434E5E";
    
    r++;
    
    if(r<=3){ 
    Apuntos[r].style.background = "#434E5E";
    Apuntos[r].style.border = "2.0px solid white";
    }
    else{
        r=0;
        Apuntos[r].style.background = "#434E5E";
        Apuntos[r].style.border = "2.0px solid white";
    }
    
        

    });


///// Esta es igual a la funcion de avanzar la imagen de arriba, pero esta retrocede la imagen y el punto utilizando el n-- y el condicional para que el indice n no sea menor a 0 asi no se pasa

retroceder.addEventListener("click",function(){
    imagenesGaleriaArriba[n].style.display = "none";
    n--;
    
    if(n>=0){
        imagenesGaleriaArriba[n].style.display = "flex"; 
        imagenesGaleriaArriba[n].classList.add("efectoGaleriaArriba");
    }
    else{
        n=3;
        imagenesGaleriaArriba[n].style.display = "flex"; 
        imagenesGaleriaArriba[n].classList.add("efectoGaleriaArriba");
    }

    Apuntos[r].style.background = "white";
    Apuntos[r].style.border = "2.0px solid #434E5E";
    
    r--;
    
    if(r>=0){ 
        Apuntos[r].style.background = "#434E5E";
        Apuntos[r].style.border = "2.0px solid white";
    }
    else{
        r=3;
        Apuntos[r].style.background = "#434E5E";
        Apuntos[r].style.border = "2.0px solid white";
    }
    
    });




///////////////////GALERIA DE ABAJO VEHICULOS DISPONIBLES/////////////////////    


//Estas variables corresponden a cada opcion de la galeria 
var autos = document.querySelector("#autos");
var monovolumenes = document.querySelector("#monovolumenes");
var suv = document.querySelector("#suv");
var pickups = document.querySelector("#pickups");

//Estas variables corresponden a cada contenedor de cada tipo de coches 
var Autoss = document.querySelector("#Autoss");
var Monovolumeness = document.querySelector("#Monovolumeness");
var Suvv = document.querySelector("#Suvv");
var Pickupp = document.querySelector("#Pickupp")

var f = 1;

var avanzar2 = document.querySelector("#avanzar2");
var retroceder2 = document.querySelector("#retroceder2"); 


var subClick1 = document.querySelector("#subClick1");
var subClick2 = document.querySelector("#subClick2");
var subClick3 = document.querySelector("#subClick3");
var subClick4 = document.querySelector("#subClick4");

var containerGL = document.querySelector("#contenedorGL");


var containerImagenesDeAbajo = document.querySelector("#containerImagenesDeAbajo");


/// Estas dos funciones son para reciclar codigo. Es para cuando hago click en alguna de las categorias de autos sacar y poner los coches que corresponden a la categoria y la primera lo que hace es sacarme la flechas en cualquier categoria que no sea autos

function sacarAR(elemento1,elemento2){
    elemento1.style.display = "none";
    elemento2.style.display = "none";
}

function mostrarOculutar(elementu1,elementu2,elementu3,elementu4){
    elementu1.style.display = "flex";
    elementu2.style.display = "none";
    elementu3.style.display = "none";
    elementu4.style.display = "none";

}


//// Esta es la funcion que corresponde a cuando se hace click en la categoria autos en la galeria de coches.

autos.onclick = function () {
    
    // todo esto es para que cuando haga click en autos se me acomoden las imagenes de los autos al orden inicial,y asi poder moverlos como si haria recargaria la pagina sino no lo hago me quedan en la posicion en la que los habia dejado antes de cambiar a otra categoria de autos 

    containerGL.style.transform = "translateX(0px)";
    

    //Esta variable es para contolar la cantidad de avanzar y retroceder en la galeria. Cuando hago click en esta categoria la pongo devuelta en 1 como estaba al principio

    f=1;
    

    //Esto me pone en visible el contenedor con los coches de la categoria seleccionada para que se muestre en pantalla, y las otras las oculta
    
    Autoss.style.display = "block";
    Suvv.style.display = "none";
    Monovolumeness.style.display = "none";
    Pickupp.style.display = "none";
    

    //Esto es para hacer aparecer las dos iconos de avanzar y retroceder

    avanzar2.style.display = "block";
    retroceder2.style.display ="block";

    
    // Esto es para hacer aparecer el efecto subrayado en el nombre de la categoria seleccionada, y elimina el efecto de las demas.

    autos.classList.add("subrayadoAutos");
    monovolumenes.classList.remove("subrayadoMonovolumenes");
    suv.classList.remove("subrayadoSuv");
    pickups.classList.remove("subrayadoPickups");
    
   
    //Posicionamiento, nada mas
    containerImagenesDeAbajo.style.marginTop = "5px";
}



//// Esta es la funcion que corresponde a cuando se hace click en la categoria monovolumenes en la galeria de coches.

monovolumenes.onclick = function() {
    //Esto es para hacer aparecer las dos iconos de avanzar y retroceder

    sacarAR(avanzar2,retroceder2);


    //Esto me pone en visible el contenedor con los coches de la categoria seleccionada para que se muestre en pantalla, y las otras las oculta

    mostrarOculutar(Monovolumeness,Autoss,Suvv,Pickupp);                             
  

    // Esto es para hacer aparecer el efecto subrayado en el nombre de la categoria seleccionada, y elimina el efecto de las demas.

    autos.classList.remove("subrayadoAutos");
    monovolumenes.classList.add("subrayadoMonovolumenes");
    suv.classList.remove("subrayadoSuv");
    pickups.classList.remove("subrayadoPickups");
                                     
    
    //Posicionamiento, nada mas

    Monovolumeness.style.marginTop = "36px";
   
    containerImagenesDeAbajo.style.marginTop = "65px";                                
                             
}



 //// Esta es la funcion que corresponde a cuando se hace click en la categoria suv en la galeria de coches.

suv.onclick = function() {
    //Esto es para hacer aparecer las dos iconos de avanzar y retroceder

    sacarAR(avanzar2,retroceder2);


    //Esto me pone en visible el contenedor con los coches de la categoria seleccionada para que se muestre en pantalla, y las otras las oculta

    mostrarOculutar(Suvv,Autoss,Monovolumeness,Pickupp);
    
    
    // Esto es para hacer aparecer el efecto subrayado en el nombre de la categoria seleccionada, y elimina el efecto de las demas.
    autos.classList.remove("subrayadoAutos");
    monovolumenes.classList.remove("subrayadoMonovolumenes");
    suv.classList.add("subrayadoSuv");
    pickups.classList.remove("subrayadoPickups");
    
    
    //Posicionamiento, nada mas

    Suvv.style.marginTop = "36px";
                
    containerImagenesDeAbajo.style.marginTop = "65px";        
}



 ///// Esta es la funcion que corresponde a cuando se hace click en la categoria pickups en la galeria de coches.

pickups.onclick = function() {
    //Esto es para hacer aparecer las dos iconos de avanzar y retroceder

    sacarAR(avanzar2,retroceder2);
    
    
    //Esto me pone en visible el contenedor con los coches de la categoria seleccionada para que se muestre en pantalla, y las otras las oculta

    mostrarOculutar(Pickupp,Autoss,Monovolumeness,Suvv);
   

    // Esto es para hacer aparecer el efecto subrayado en el nombre de la categoria seleccionada, y elimina el efecto de las demas.

    autos.classList.remove("subrayadoAutos");
    monovolumenes.classList.remove("subrayadoMonovolumenes");
    suv.classList.remove("subrayadoSuv");
    pickups.classList.add("subrayadoPickups");
                                                        
    
    //Posicionamiento, nada mas

    Pickupp.style.marginTop = "36px";
   
    containerImagenesDeAbajo.style.marginTop = "65px";
}



///// Apartir del ancho 504 hasta llegar al 720px, voy usando distintas valores de traslateX para que me queden bien centrados los coches que se van mostrando. Es por ello que guarde en dos variables los valores de traslateX de avanzar y retroceder que nesecito en cada ancho de pantalla. Esto lo hice que se ejecute cada ves que inicio la pagina y cada ves que cambio el tamaño de pantalla

var ValorTraslateXAvanzar;
var ValorTraslateXRetroceder;
var numeroMaximoIndiceGaleriaCoches;

function CondicionValorTraslatexSegunSize (windowSizee, desde, hasta, valorAvanzar, valorRetroceder, indice ) {
    if(windowSizee>=desde && windowSizee<=hasta ){
        ValorTraslateXAvanzar = valorAvanzar;
        ValorTraslateXRetroceder = valorRetroceder;
        numeroMaximoIndiceGaleriaCoches = indice;
     }
}

function ValoresParaTraslateXGaleriaCoches () {
    autos.classList.add("subrayadoAutos");

    let windowSize = window.innerWidth;
    
    CondicionValorTraslatexSegunSize (windowSize, 0, 504, "translateX(-269px)", "translateX(269px)", 6); 
    CondicionValorTraslatexSegunSize (windowSize, 504, 546, "translateX(-213px)", "translateX(213px)", 5); 
    CondicionValorTraslatexSegunSize (windowSize, 546, 636, "translateX(-240px)", "translateX(240px)", 5);
    CondicionValorTraslatexSegunSize (windowSize, 636, 720, "translateX(-275px)", "translateX(275px)", 5); 
    CondicionValorTraslatexSegunSize (windowSize, 720, 780, "translateX(-310px)", "translateX(310px)", 5); 
    CondicionValorTraslatexSegunSize (windowSize, 780, 840, "translateX(-330px)", "translateX(330px)", 5);
    CondicionValorTraslatexSegunSize (windowSize, 840, 900, "translateX(-350px)", "translateX(350px)", 5); 
    CondicionValorTraslatexSegunSize (windowSize, 900, 960, "translateX(-370px)", "translateX(370px)", 5);
    CondicionValorTraslatexSegunSize (windowSize, 960, 1020, "translateX(-390px)", "translateX(390px)", 5);
    CondicionValorTraslatexSegunSize (windowSize, 1020, 1080, "translateX(-295px)", "translateX(295px)", 4);
    CondicionValorTraslatexSegunSize (windowSize, 1080, 1160, "translateX(-318px)", "translateX(318px)", 4);
    CondicionValorTraslatexSegunSize (windowSize, 1160, 1240, "translateX(-341px)", "translateX(341px)", 4);
    CondicionValorTraslatexSegunSize (windowSize, 1240, 1280, "translateX(-364px)", "translateX(364px)", 4);
    CondicionValorTraslatexSegunSize (windowSize, 1280, 1320, "translateX(-269px)", "translateX(269px)", 3);
    CondicionValorTraslatexSegunSize (windowSize, 1320, 1360, "translateX(-289px)", "translateX(289px)", 3);
    CondicionValorTraslatexSegunSize (windowSize, 1360, 1400, "translateX(-304px)", "translateX(304px)", 3);
    CondicionValorTraslatexSegunSize (windowSize, 1400, 1440, "translateX(-315px)", "translateX(315px)", 3);
    CondicionValorTraslatexSegunSize (windowSize, 1440, 122440, "translateX(-269px)", "translateX(269px)", 3);
}

window.addEventListener('load', ValoresParaTraslateXGaleriaCoches);

function VvaloresParaTraslateXGaleriaCoches () {
    let windowSize = window.innerWidth;
     
    CondicionValorTraslatexSegunSize (windowSize, 0, 504, "translateX(-269px)", "translateX(269px)", 6); 
    CondicionValorTraslatexSegunSize (windowSize, 504, 546, "translateX(-213px)", "translateX(213px)", 5); 
    CondicionValorTraslatexSegunSize (windowSize, 546, 636, "translateX(-240px)", "translateX(240px)", 5);
    CondicionValorTraslatexSegunSize (windowSize, 636, 720, "translateX(-275px)", "translateX(275px)", 5); 
    CondicionValorTraslatexSegunSize (windowSize, 720, 780, "translateX(-310px)", "translateX(310px)", 5); 
    CondicionValorTraslatexSegunSize (windowSize, 780, 840, "translateX(-330px)", "translateX(330px)", 5);
    CondicionValorTraslatexSegunSize (windowSize, 840, 900, "translateX(-350px)", "translateX(350px)", 5); 
    CondicionValorTraslatexSegunSize (windowSize, 900, 960, "translateX(-370px)", "translateX(370px)", 5);
    CondicionValorTraslatexSegunSize (windowSize, 960, 1020, "translateX(-390px)", "translateX(390px)", 5);
    CondicionValorTraslatexSegunSize (windowSize, 1020, 1080, "translateX(-295px)", "translateX(295px)", 4);
    CondicionValorTraslatexSegunSize (windowSize, 1080, 1160, "translateX(-318px)", "translateX(318px)", 4);
    CondicionValorTraslatexSegunSize (windowSize, 1160, 1240, "translateX(-341px)", "translateX(341px)", 4);
    CondicionValorTraslatexSegunSize (windowSize, 1240, 1280, "translateX(-364px)", "translateX(364px)", 4);
    CondicionValorTraslatexSegunSize (windowSize, 1280, 1320, "translateX(-269px)", "translateX(269px)", 3);
    CondicionValorTraslatexSegunSize (windowSize, 1320, 1360, "translateX(-289px)", "translateX(289px)", 3);
    CondicionValorTraslatexSegunSize (windowSize, 1360, 1400, "translateX(-304px)", "translateX(304px)", 3);
    CondicionValorTraslatexSegunSize (windowSize, 1400, 1440, "translateX(-315px)", "translateX(315px)", 3);
    CondicionValorTraslatexSegunSize (windowSize, 1440, 122440, "translateX(-269px)", "translateX(269px)", 3);
}

window.addEventListener('resize', VvaloresParaTraslateXGaleriaCoches);



///// Esta funcion es para hacer retroceder la galeria de coches cuando se hce click en el icono de atras

retroceder2.onclick = function(){  
    //Me reduce en uno la variable indice
    f--;
    

    //Si el ancho es mayor a 504 quiere decir que esta en la vista en la que se ven 2 autos en el slide de la galeria. Si el indice es menor a 0 quiere decir que esta en el primer auto y pongo el f = 0 para que no retroceda mas, caso contrario si retrocede segun el valor de traslateX correspondiente.

    
        if (f<0) {  
            f=0;
        } 
        else {//213 504,  240 636 ,
            containerGL.style.transform += ValorTraslateXRetroceder;
            flagMovioGleriaTamaño504 = true;
        }
    

    // Si el ancho no es mayor a 504px quiere decir que esta en el modo mobil vertical en donde se ve un solo auto en la galeria, por lo que utilizo un valor de traslateX distinto. Uso el mismo valor para cuando es mayor a 720px y hago como el true lo poner el indice en 0 cuando llega al primer coche

   
    //containerGL.style.transform += "translateX(195px)"; -200
}



///// Esta funcion es para hacer adelantar la galeria de coches cuando se hce click en el icono de adelantar

avanzar2.onclick = function(){
    
    
    // Si el tamaño de pantalla es menor a 720px, y menor a 504, quiere decir que solo se muestra un solo coche en la galeria, por lo que la cantidad de clicks para avanzar y retroceder aumenta por lo que uso como limite el 6. En cambio si es mayor a 504 y menor a 720 quiere decir que se muestran dos coches, y en este caso la cantidad limite de clicks son 5 y uso el valor de traslateX que corresponda

       f++;
     
       if (f>numeroMaximoIndiceGaleriaCoches) {
           f=numeroMaximoIndiceGaleriaCoches; 
       } 
       else {
          containerGL.style.transform += ValorTraslateXAvanzar; 
          flagMovioGleriaTamaño504 = true;
       }
           
    

    // Si va por este ldo quiere decir que la cantidad maxima de clicks son 4
   
   /* else{
        f++;

        if (f>4){
            f=4
        }
         else {
            containerGL.style.transform += "translateX(-269px)"; 
            flagMovioGleriaTamañoMenor504 = true;
        }   
    }
    */   
}



///// Esto es para que cuando el tamaño de la ventana sea mayor a 720px, ir acomodando los indices segun la posicion en la que estan para que no se pasen al tener otro tamaño de pantalla con otra cantidad de clicks, y tambien la posicion del coche que esta mostrando corresponda a la posicion del indice. Cuando es menor a 720, al mover el tamaño de la ventana directamente inicializo ambos valores a como estabn en un principio
  
function GaleriaAbajoTamañoMobil () {
    

           f=1;
        containerGL.style.transform = "translateX(0px)";
    

    
}

window.addEventListener('resize', GaleriaAbajoTamañoMobil);
        






//////////////////////MENU DESPLEGABLE VEHICULOS FOOTER/////////////////////////


var body = document.querySelector("#body");
var elegirModeloFooter = document.querySelector("#elegir-modelo-footer");
var desplegableFooter = document.querySelector("#desplegable-footer");

elegirModeloFooter.onclick = function(){

     
    desplegableFooter.classList.toggle("mostrar-desplegable"); 
    



};




///////////////////EFECTOS SCROLL IMAGENES DE ABAJO Y TEXTO DE ABAJO //////////////


// primero traigo todos los elementos a los que les voy a aplicar animacion al hacer scroll, los pongo a todos con un mismo nombre de clase y esa clase scroll en css tiene opaquisisdad 0 y transicion

var elementosScroll = document.querySelectorAll(".scroll"); 
var animacionTextos1a = document.querySelector("#animacionTextos1a");
var animacionTextos1b = document.querySelector("#animacionTextos1b");
var animacionTextos2a = document.querySelector("#animacionTextos2a");
var animacionTextos2b = document.querySelector("#animacionTextos2b");


function efectoScroll () {


    //con esto guardo en la variable scrollTop la posicion del eje y en la que se encuentra la pantalla posicionada 

    var scrollTop = document.documentElement.scrollTop; 


    //con esto trabajo todos los elementos a los que le quiero hacer scroll y les puedo aplicar estilos y efectos a todos por igual o voy seleccionando con el if los elementos y el i que es la posicion en la que esta

    for (i=0; i<elementosScroll.length; i++){


        //pongo eso que me guarda en alturaelemento la posicion del eje y en la que se encuentra en elemento que se esta laburando

        var alturaElemento = elementosScroll[i].offsetTop; 
        

        // le sigue un if en el que diga que si la posicion del eje y del elemento con el que se esta trabajando es menor a scroltop que es la posicion de la pantalla o del scroll que me aplique la opaquisidad 1 para que aparesca y me agregue esas clases de keyfram para que aparescan por los costados y el 600 es para la posicion del elemento menos 600 asi el efecto empieza cuando el scroll va un poco mas arriba del elemento

        if (alturaElemento -600 < scrollTop) { 
            header.style.top= "-100px";
            elementosScroll[i].style.opacity = 1;
            animacionTextos1a.classList.add("animacionTextos1");
            animacionTextos1b.classList.add("animacionTextos1");
            if (i>2){ 
            animacionTextos2a.classList.add("animacionTextos2");
            animacionTextos2b.classList.add("animacionTextos2");
            }    
        
            
        } } }
window.addEventListener("scroll",efectoScroll);




////////////////////////EFECTO QUE DESAPARESCA Y APARESCA EL MENU CON EL SCROLL///////////////


var flagTamañoMobil;
var flagTamañoMobilHorizontal;

////// Estas dos funciones son para determinar el tamaño de la pantalla al cargar la pagina y al cambiar el tamaño de la ventana. Con esa informacion luego puedo determinar en que posicion del eje y voy a desplegar y retraer el menu
function ValorFlagTamañoMobil () {
    let windowSize = window.innerWidth;
    
    if(windowSize<480){
       flagTamañoMobil = true;
    }
    else{
        flagTamañoMobil = false;
    }

    if(windowSize>=480 && windowSize<=720 ){
        flagTamañoMobilHorizontal = true;
     }
     else{
         flagTamañoMobilHorizontal = false;
     }
}

window.addEventListener('load', ValorFlagTamañoMobil);

function VvalorFlagTamañoMobil () {
    let windowSize = window.innerWidth;
    
    if(windowSize<480){
        flagTamañoMobil = true;
     }
     else{
         flagTamañoMobil = false;
     }

     if(windowSize>=480 && windowSize<=720 ){
        flagTamañoMobilHorizontal = true;
     }
     else{
         flagTamañoMobilHorizontal = false;
     }
     
}

window.addEventListener('resize', VvalorFlagTamañoMobil);


//lo que hago en ambos es reutilizar el scroll que habia usado antes, solo le cambio el -600 por -900

 
///// Funcion para hacer desaparecer el menu cuando hago scroll para abajo. Funciona igual que l funcion EfectoScroll, ahi esta bien explicada. La diferencia es que aca uso condiciones para saber en que tamaño de pantalla estoy y usar cordenadas distintas 

function desaparecermenu () {
    
    var scrollTop = document.documentElement.scrollTop;
    
    for (i=0; i<elementosScroll.length; i++){
        var alturaElemento = elementosScroll[i].offsetTop; 
        
        if (flagTamañoMobil==true){ 
            if (alturaElemento -1950 < scrollTop) {  
                header.style.top= "-333px";
                menu.classList.remove("mostrar");
            }
        }

        if (flagTamañoMobilHorizontal==true){
            if (alturaElemento -1700 < scrollTop) {  
                header.style.top= "-333px";
                menu.classList.remove("mostrar");
            }
        
        }

        if (flagTamañoMobilHorizontal==false && flagTamañoMobil==false){
            if (alturaElemento -1300 < scrollTop) {  
                header.style.top= "-333px";
                menu.classList.remove("mostrar");
            }
        
        }
        
    }  
}  

    
window.addEventListener("scroll",desaparecermenu);


///// Es igual que la anterior, pero esta es para hacer aparecer el menu y es cuando se hace scroll hacia arriba.

function aparecerMenu () {
    
    var scrollTop = document.documentElement.scrollTop;
    
    for (i=0; i<elementosScroll.length; i++){
        var alturaElemento = elementosScroll[i].offsetTop; 
        
        if (flagTamañoMobil==true){ 
            if (alturaElemento -1950 > scrollTop) {  
                header.style.top= "0px";
                
            }
        }

        if (flagTamañoMobilHorizontal==true){
            if (alturaElemento -1700 > scrollTop) {  
                header.style.top= "0px";
            }
        
        }

        if (flagTamañoMobilHorizontal==false && flagTamañoMobil==false){
            if (alturaElemento -1300 > scrollTop) {  
                header.style.top= "0px";
            }
        
        }
        
    }  
}  

    
window.addEventListener("scroll",aparecerMenu);







